# Write a simple, short program to calculate and print 
# area of one of the biggest faces and 
# volume of a rectangular cuboid with the dimensions given below

# Length (l) = 30cm
# Breadth (b) = 20cm
# Height (h) = 15cm
## %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ##
# initialize variables

l = 30
b = 20
h = 15

# python program to find  area of one of the biggest faces a rectangular cuboid (core functionality)
face_one = l*b
face_two =b*h
face_three = l*h
area_bigg_face_rect_cuboid = max(face_one,face_two,face_three)
print("The area of one of the biggest faces of a Rectangular Cuboid",area_bigg_face_rect_cuboid )


# python program to find  volume of a rectangular cuboid
volume_cuboid = l*b*h
print("The Volume of a Rectangular Cuboid =",volume_cuboid)

