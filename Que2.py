# Write a simple, short program to calculate and print 
# area of one of the biggest faces and 
# volume of a rectangular cuboid with the dimensions given below
# Use functions and loops if required

# Length (l) = [30cm,40cm,50cm]
# Breadth (b) = 20cm
# Height (h) = 15cm
## %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ##


# python program to find  area of one of the biggest faces a rectangular cuboid
def area_rect_cuboid(l,b,h):
    face_one=l*b
    face_two=b*h
    face_three=l*h
    area_bigg_face_rect_cuboid = max(face_one,face_two,face_three)        
    return area_bigg_face_rect_cuboid

l = [30,40,50]
b = 20
h = 15    
for i in l:
    print(area_rect_cuboid(i,b,h))




# python program to find  volume of a rectangular cuboid
def Volume_RectCuboid(l,b,h):
    volume_cuboid = l*b*h    
    return volume_cuboid
l = [30,40,50]
b = 20
h = 15    
for i in l:      
    print(Volume_RectCuboid(i,b,h))
